class APP {
  final String name;
  final String sector;
  final String developer;
  final int year;

  APP(this.name, this.sector, this.developer, this.year);

}

final a1 = APP('Takealot', 'e-commerce', 'Takealot', 2021);

main() {
  print(a1.toString()); //Instance of 'App'
  print(a1); //Instance of 'App'
}